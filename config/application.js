/* 
 * Copyright (C) MOTHER - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by 
 * Mac Neil Ivan Tumacay <tumacayivan@gmail.com>, 04/20/2021
 */

function getConfig() {
  return {
    env: 'test',
    name: 'Base',
    jwt: {
      header: 'Authorization',
      prefix: 'Bearer',
      issuer: 'APN',
      subject: 'Base Authentication',
      audience: 'Base',
      key: '$2a$10$FwJD/nVbPz3xDsorrd/oPehALWD267X0EHHTfmykrK8Xaz..BkSsC',
      expiry: {
        min: 1800000,
        mid: 86400000,
        max: 172800000
      }
    }
  };
}

module.exports = { getConfig };