/* 
 * Copyright (C) MOTHER - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by 
 * Mac Neil Ivan Tumacay <tumacayivan@gmail.com>, 04/20/2021
 */

module.exports = {
	get:function(req, res, next){
 		console.log('GET all data');
 		// let ip = req.headers['cf-connecting-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
 		// console.log(ip);
 		// let requestIp = require('request-ip');
 		// let clientIp = requestIp.getClientIp(req); 
 		

 	// 	// Authorization
		// if(req.headers.authorization == undefined) {
		// 	res.status(200).json({
	 // 			timestamp: Math.floor(Date.now()).toString(),
		// 		status: "200",
		// 		message: 'Request processed successfully',
		// 		path: req.originalUrl,
		// 		data: {
		// 			status_description: "Transaction Failed",
		// 			status_code: "ER.00.00",
		// 			error: "No Authorization"
		// 		}
		// 	});
		// 	res.sendStatus(200);
		// } 
		
 	// 	let jwtUtil = require('../models/jwt.js');
		// jwtUtil.verify({
		// 	token: req.headers.authorization
		// }, function(response) {
		// 	if(response.data == undefined) {
		// 		// Transaction Failed
		// 		res.status(200).json({
		//  			timestamp: Math.floor(Date.now()).toString(),
		// 			status: "200",
		// 			message: 'Request processed successfully',
		// 			path: req.originalUrl,
		// 			data: {
		// 				status_description: "Transaction Failed",
		// 				status_code: "ER.00.00",
		// 				error: "Invalid Authorization"
		// 			}
		// 		});
		// 	} else {
		// 		// Verification Successful

		// 	}
		// });

 		res.status(200).json({
 			timestamp: Math.floor(Date.now()).toString(),
			status: "200",
			message: 'Request processed successfully',
			path: req.originalUrl,
			data: {
				status_description: "Transaction Successful",
				status_code: "OK.00.00"
			}
		});
	},
	getById:function(req, res, next){
 		console.log('GET data by Id');
 		console.log(req.params.id);
 		res.status(200).json({
 			timestamp: Math.floor(Date.now()).toString(),
			status: "200",
			message: 'Request processed successfully',
			path: req.originalUrl,
			data: {
				status_description: "Transaction Successful",
				status_code: "OK.00.00"
			}
		});
	},
	save:function(req, res, next){
		console.log('POST a data');
		console.log(req.body);

		const { Configuration, OpenAIApi } = require("openai")

		const configuration = new Configuration({
		  apiKey: "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"
		  // apiKey: "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" 
		})
		const openai = new OpenAIApi(configuration)

		async function go() {
		  const completion = await openai.createCompletion({
		    model: "text-davinci-003",
		    prompt: req.body.prompt,
		    temperature: 0,
		    max_tokens: 4000
		  })
		  // console.log(completion.data.choices[0].text)

		  res.status(200).json({
	 			timestamp: Math.floor(Date.now()).toString(),
				status: "200",
				message: 'Request processed successfully',
				path: req.originalUrl,
				data: {
					status_description: "Transaction Successful",
					status_code: "OK.00.00",
					reply: completion.data.choices[0].text
				}
			});
		}

		go()

		// res.status(200).json({
 		// 	timestamp: Math.floor(Date.now()).toString(),
		// 	status: "200",
		// 	message: 'Request processed successfully',
		// 	path: req.originalUrl,
		// 	data: {
		// 		status_description: "Transaction Successful",
		// 		status_code: "OK.00.00"
		// 	}
		// });
  	},
	update:function(req, res, next){
		console.log('UPDATE data by Id');
		console.log(req.params.id);
		console.log(req.body);
		res.status(200).json({
 			timestamp: Math.floor(Date.now()).toString(),
			status: "200",
			message: 'Request processed successfully',
			path: req.originalUrl,
			data: {
				status_description: "Transaction Successful",
				status_code: "OK.00.00"
			}
		});
  	},
	delete:function(req, res, next){
		console.log('DELETE data by Id');
		console.log(req.params.id);
		res.status(200).json({
 			timestamp: Math.floor(Date.now()).toString(),
			status: "200",
			message: 'Request processed successfully',
			path: req.originalUrl,
			data: {
				status_description: "Transaction Successful",
				status_code: "OK.00.00"
			}
		});
  	}
}