<!-- Install the Nodemon -->
npm install nodemon -g 
nodemon ./app.js localhost 8080

<!-- Install the Heroku CLI -->
Download and install the Heroku CLI.

If you haven't already, log in to your Heroku account and follow the prompts to create a new SSH public key.

$ heroku login
Clone the repository
Use Git to clone reward-point-system's source code to your local machine.

$ heroku git:clone -a reward-point-system
$ cd reward-point-system
Deploy your changes
Make some changes to the code you just cloned and deploy them to Heroku using Git.

$ git add .
$ git commit -am "make it better"
$ git push heroku master


ACCOUNT 1
-MZNn7OADb4gklQnDqnr
https://reward-point-system.herokuapp.com/web/?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhY2NvdW50X2lkIjoiLU1aTm43T0FEYjRna2xRbkRxbnIiLCJpYXQiOjE2MTk2MTg3MjgsImV4cCI6NDc3MzIxODcyOH0.BXppbKjpdteBX5IW-E75SnCmqwWfvT4uH1rAaRwDOwY

http://localhost:8080/web/?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhY2NvdW50X2lkIjoiLU1aTm43T0FEYjRna2xRbkRxbnIiLCJpYXQiOjE2MTk2MTg3MjgsImV4cCI6NDc3MzIxODcyOH0.BXppbKjpdteBX5IW-E75SnCmqwWfvT4uH1rAaRwDOwY

ACCOUNT 2
-MZkE_ng66pDVy4xGlrm
https://reward-point-system.herokuapp.com/web/?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhY2NvdW50X2lkIjoiLU1aa0Vfbmc2NnBEVnk0eEdscm0iLCJpYXQiOjE2MjAwMTIwNjQsImV4cCI6NDc3MzYxMjA2NH0.zMP89IF-phOGvDRkCXolZaPVrJO_OdH1hhOWNf6XSTs


ACCOUNT 3
-MZkE_ndzsdV4ayv9_gE
https://reward-point-system.herokuapp.com/web/?token=eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJhY2NvdW50X2lkIjoiLU1aa0VfbmR6c2RWNGF5djlfZ0UiLCJpYXQiOjE2MjAwMTIwNjQsImV4cCI6NDc3MzYxMjA2NH0.FhzXBlZswBLhLecgH8C9MRcT4qckuZF7qRV-cmHazaM

